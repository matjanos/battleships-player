﻿namespace Battleships.Modules.KubaPlayer
{
    internal enum SquereStatus
    {
        /// <summary>
        /// Not checked yet
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Striked successfully
        /// </summary>
        Hit = 1,

        /// <summary>
        /// Battleship can't be here because of battle rules
        /// </summary>
        Impossible = 2,
        
        /// <summary>
        /// Striked and missed
        /// </summary>
        Missed = 3,
    }
}