﻿using System;
using System.Collections.Generic;
using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer.Strategies
{
    class PlayerChooseStrategy : IPlayerChooseStrategy
    {
        private IList<PlayerInfo> opponents;
        private Random rand = new Random();

        public PlayerChooseStrategy(IList<PlayerInfo> opponents)
        {
            this.opponents = opponents;
        }

        public void UpdateStrategyKnowledge(IList<PlayerInfo> playersInfos)
        {
             this.opponents = playersInfos;
        }

        public PlayerInfo GetNextPlayer()
        {
            return opponents[rand.Next(0, opponents.Count - 1)];
        }
    }
}