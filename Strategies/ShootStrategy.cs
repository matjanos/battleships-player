using System;
using System.Collections.Generic;
using System.Linq;
using Battleships.Gameplay;
using Microsoft.Practices.Unity.Utility;

namespace Battleships.Modules.KubaPlayer.Strategies
{
    public class ShootStrategy : IShootStrategy
    {
        private readonly IDictionary<ShipOrientation, int> orientationStatistics;
        private readonly IList<ShipPosition> unusedPositions;
        private SquereStatus[,] currentBoard;
        private readonly IList<IBattleship> fleetLeft;
        private readonly PlayerInfo myPlayerInfo;
        private readonly Random randomizer = new Random();

        private IDictionary<PlayerInfo,Pair<bool,BombardedBattleshipInfo>> cannonades = new Dictionary<PlayerInfo, Pair<bool, BombardedBattleshipInfo>>();

        public ShootStrategy(int rows, int columns, IList<IBattleship> fleet, PlayerInfo myPlayerInfo)
        {
            currentBoard = new SquereStatus[rows, columns];
            unusedPositions = CreateUnusedPositions(rows, columns);
            fleetLeft = fleet;
            this.myPlayerInfo = myPlayerInfo;
            orientationStatistics = new Dictionary<ShipOrientation, int>
            {
                {ShipOrientation.Horizontal, 0},
                {ShipOrientation.Vertical, 0},
            };
        }

        public ShipPosition GetRandomNotUsedInTrouble()
        {
             var nextToShoot = unusedPositions[randomizer.Next(unusedPositions.Count - 1)];
            unusedPositions.Remove(nextToShoot);

            return nextToShoot;
        }

        public ShipPosition GetNextPosition()
        {
            ShipPosition nextToShoot;
            if (cannonades.ContainsKey(myPlayerInfo))
            {
                nextToShoot = cannonades[myPlayerInfo].Second.GetNextPosition();
            }
            else
            {
                nextToShoot = unusedPositions[randomizer.Next(unusedPositions.Count - 1)];
            }
            unusedPositions.Remove(nextToShoot);

            return nextToShoot;

        }

        public void UpdateHistory(ShipPosition position, bool wasHit, bool isSunk, PlayerInfo source)
        {
            if (!cannonades.ContainsKey(source))
            {
                if (wasHit)
                {
                    cannonades.Add(source,new Pair<bool, BombardedBattleshipInfo>(true,GuessBattleship(position)));
                }
            }
            else
            {
                if (wasHit)
                {
                    cannonades[source].Second.ShipCurrentSize++;
                }
            }
            if (cannonades.ContainsKey(source))
                cannonades[source].Second.SaveResult(position, wasHit);

            if (isSunk)
            {
                var shipToDelete =
                    fleetLeft
                        .FirstOrDefault(battleship => battleship.Size == cannonades[source].Second.ShipCurrentSize);
                if (shipToDelete != null)
                {
                    fleetLeft.Remove(shipToDelete);
                    UpdateStatistics(shipToDelete);
                }

                var mergeCandidates = cannonades.Where(element => element.Value.Second.CanMerge(cannonades[source].Second)).Select(pair => pair.Key);

                cannonades.Remove(source);
                foreach (var mergeCandidate in mergeCandidates)
                {
                    cannonades.Remove(mergeCandidate);
                }

            }
        }

        private void UpdateStatistics(IBattleship sunkShip)
        {
            //TODO
        }

        private static IList<ShipPosition> CreateUnusedPositions(int rows, int columns)
        {
            IList<ShipPosition> pos = new List<ShipPosition>(rows * columns);
            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= columns; j++)
                {
                    pos.Add(new ShipPosition(i, j));
                }
            }
            return pos;
        }

        private BombardedBattleshipInfo GuessBattleship(ShipPosition position)
        {
            // rozkmini� takie rzeczy jak odleg�o�ci od kraw�dzi, od innych statk�w, por�wna� z zatopion� flot�.
            //narazie kompletna kupa
            return new BombardedBattleshipInfo(currentBoard.GetLength(1), currentBoard.GetLength(0))
            {
                ShipCurrentSize = 1,

                PotencialOrientation =
                    orientationStatistics[ShipOrientation.Horizontal] > orientationStatistics[ShipOrientation.Vertical]
                        ? ShipOrientation.Horizontal
                        : ShipOrientation.Vertical,
                
            };
        }
    }
}