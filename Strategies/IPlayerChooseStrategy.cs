﻿using System.Collections.Generic;
using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer.Strategies
{
    interface IPlayerChooseStrategy
    {
        void UpdateStrategyKnowledge(IList<PlayerInfo> playersInfos);

        PlayerInfo GetNextPlayer();
    }
}
