﻿using System.Collections.Generic;
using System.Linq;
using Battleships.Gameplay;
using Battleships.Gameplay.Exceptions;
using System;
using Battleships.Modules.KubaPlayer;
using Battleships.Modules.KubaPlayer.Strategies;

namespace Battleships.Modules.ReferencePlayers
{
    public class RandomShipPlacingStrategy : IPlacingStrategy
    {
        readonly Random random = new Random();

        protected void PlaceBattleship(IBattleship ship, IBoard board, ShipPosition position)
        {
            board.Place(ship, position, random.Next(0,1)==1?ShipOrientation.Horizontal : ShipOrientation.Vertical);
        }

        private bool TryPlaceBattleship(IBattleship ship, IBoard board, ShipPosition position)
        {
            try
            {
                PlaceBattleship(ship, board, position);
                return true;
            }
            catch (OverlayingBattleshipsBoardException)
            {
                return false;
            }
            catch (BattleshipOutOfBoardException)
            {
                return false;
            }
        }

        private IEnumerable<ShipPosition> GetAllPositionsList(IBoard board)
        {
            var positions = new List<ShipPosition>();
            for (int row = 1; row <= board.Rows; row++)
            {
                for (int column = 1; column <= board.Columns; column++)
                {
                    positions.Add(new ShipPosition(row, column));
                }
            }
            positions.Shuffle();

            return positions;
        }

        public void PlaceFleet(IGameplayBoard board, IList<IBattleship> fleet)
        {
            var positions = new Queue<ShipPosition>(GetAllPositionsList(board));

            foreach (var battleship in fleet.OrderByDescending(b => b.Size))
            {
                while (!TryPlaceBattleship(battleship, board, positions.Dequeue())) ;
            }
        }
    }
}