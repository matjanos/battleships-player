﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Battleships.Gameplay;
using Battleships.Modules.KubaPlayer.Strategies;
using Battleships.Modules.ReferencePlayers;
using log4net;

using log4net.Core;

namespace Battleships.Modules.KubaPlayer
{
    public class KubaPlayer : IGameplayPlayer
    {
        public static string PlayerName = "KubaPlayer";
        Semaphore sem = new Semaphore(1,1,"Shooting");

        private static readonly ILog logger = LogManager.GetLogger(typeof(KubaPlayer));

        public string Name { get { return PlayerName; } }
        private readonly Random randomizer = new Random();
        private readonly IDictionary<PlayerInfo, SquereStatus[,]> opponentsBoards;//TODO: czy to w ogóle potrzebne? - odpowiedzialność strategii
        private IList<PlayerInfo> opponents;
        private PlayerInfo myInfo;

        private IPlayerChooseStrategy playerChooseStrategy;
        private readonly IPlacingStrategy placingStrategy = new RandomShipPlacingStrategy();

        private readonly IDictionary<PlayerInfo, IShootStrategy> shootStrategies = new Dictionary<PlayerInfo, IShootStrategy>();


        public KubaPlayer()
        {
            opponentsBoards = new Dictionary<PlayerInfo, SquereStatus[,]>();
        }

        public void Setup(IBattle battle, IBoard board, IList<IBattleship> fleet, IList<PlayerInfo> players, PlayerInfo me)
        {
            AssingBatteleEventsHandlers(battle);
            myInfo = me;
            this.Board = (IGameplayBoard)board;
            opponents = players.Except(new[] { me }).ToList();
            playerChooseStrategy = new PlayerChooseStrategy(players.Where(p => !Equals(p, myInfo)).ToArray());
            foreach (var playerInfo in opponents)
            {
                opponentsBoards.Add(playerInfo, new SquereStatus[board.Rows, board.Columns]);
                shootStrategies.Add(playerInfo, new ShootStrategy(board.Rows, board.Columns, fleet));

            }

            placingStrategy.PlaceFleet(Board, fleet);

        }

        private void AssingBatteleEventsHandlers(IBattle battle)
        {
            battle.BattleCrashed += BattleCrashedHandler;
            battle.BattleEnded += BattleEndedHandler;
            battle.BattleStarted += BattleStartedHandler;
            battle.CannonballFired += CannonballFiredHandler;
            battle.NewRound += NewRoundHandler;
            battle.PlayerDead += PlayerDeadHandler;
        }

        void PlayerDeadHandler(PlayerInfo player)
        {
            //opponents.Remove(player);
            //shootStrategies.Remove(player);
        }

        void NewRoundHandler(int roundNumber)
        {
            logger.Debug(String.Format("Battle {0}", roundNumber));
        }

        void CannonballFiredHandler(Cannonball cannonball, bool wasHit, bool wasSunk)
        {
            try
            {
                if (cannonball.TargetPlayer.Equals(myInfo))
                    return;

                var position = cannonball.Position;
                shootStrategies[cannonball.TargetPlayer].UpdateHistory(position, wasHit, wasSunk);

                if (wasHit)
                    opponentsBoards[cannonball.TargetPlayer][position.Row - 1, position.Column - 1] = SquereStatus.Hit;
                else
                    opponentsBoards[cannonball.TargetPlayer][position.Row - 1, position.Column - 1] =
                        SquereStatus.Missed;
            }
            catch (Exception ex)
            {
                logger.Error("Cannonball exeption", ex);
            }
            finally
            {
            }
        }

        void BattleStartedHandler()
        {
            logger.Info(String.Format("Battle Started"));
        }

        void BattleEndedHandler(IList<PlayerInfo> winners)
        {
            logger.Info(String.Format("Battle ended"));
        }

        void BattleCrashedHandler(string reason)
        {
            logger.Warn(reason);
        }

        public void Cleanup()
        {
            opponentsBoards.Clear();
        }

        public Cannonball FireCannonball()
        {
            try
            {
                var nextPlayer = playerChooseStrategy.GetNextPlayer();
                var cannball = new Cannonball(myInfo, nextPlayer, shootStrategies[nextPlayer].GetNextPosition());
                if (cannball.Position.Column > Board.Columns || cannball.Position.Column < 1 ||
                    cannball.Position.Row > Board.Rows || cannball.Position.Row < 1)
                {
                    cannball = new Cannonball(myInfo, nextPlayer, shootStrategies[nextPlayer].GetNextPosition());
                }
                lastShoot = cannball;
                return cannball;
            }
            catch (Exception ex)
            {
                logger.Error("Cannonball fire exception", ex);
                return new Cannonball();
            }
            finally
            {
            }
        }

        private Cannonball lastShoot;

        public IGameplayBoard Board { get; private set; }
    }
}
