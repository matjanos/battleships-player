﻿using System.Collections.Generic;
using System.Linq;
using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer.Strategies
{
    class PlayerChooseStrategy : IPlayerChooseStrategy
    {
        private readonly IList<PlayerInfo> opponents;

        public PlayerChooseStrategy(IList<PlayerInfo> opponents)
        {
            this.opponents = opponents;
        }

        public void UpdateStrategyKnowledge(IList<PlayerInfo> playersInfos)
        {
          //  throw new System.NotImplementedException();
        }

        public PlayerInfo GetNextPlayer()
        {
            return opponents.First();
        }
    }
}