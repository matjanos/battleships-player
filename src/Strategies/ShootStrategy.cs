using System;
using System.Collections.Generic;
using System.Linq;
using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer.Strategies
{
    public class ShootStrategy : IShootStrategy
    {
        private BombardedBattleshipInfo currentlyBombardedShip;
        private bool isCannonadeModeOn;
        private readonly IDictionary<ShipOrientation, int> orientationStatistics;
        private readonly IList<ShipPosition> unusedPositions;
        private SquereStatus[,] currentBoard;
        private readonly IList<IBattleship> fleetLeft;
        private readonly Random randomizer = new Random();


        public ShootStrategy(int rows, int columns, IList<IBattleship> fleet)
        {
            currentBoard = new SquereStatus[rows, columns];
            unusedPositions = CreateUnusedPositions(rows, columns);
            fleetLeft = fleet;
            orientationStatistics = new Dictionary<ShipOrientation, int>
            {
                {ShipOrientation.Horizontal, 0},
                {ShipOrientation.Vertical, 0},
            };
        }

        public ShipPosition GetNextPosition()
        {
            ShipPosition nextToShoot;
            if (isCannonadeModeOn)
            {
                nextToShoot = currentlyBombardedShip.GetNextPosition();
            }
            else
            {
                nextToShoot = unusedPositions[randomizer.Next(unusedPositions.Count - 1)];
            }
            unusedPositions.Remove(nextToShoot);

            return nextToShoot;

        }

        public void UpdateHistory(ShipPosition position, bool wasHit, bool isSunk)
        {
            if (currentlyBombardedShip == null)
            {
                if (wasHit)
                {
                    isCannonadeModeOn = true;
                    currentlyBombardedShip = GuessBattleship(position);//TODO: ten sam?
                }
            }
            else
            {
                if (wasHit)
                {
                    currentlyBombardedShip.ShipCurrentSize++;
                }
            }
            if (currentlyBombardedShip != null)
                currentlyBombardedShip.SaveResult(position, wasHit);

            if (isSunk)
            {
                isCannonadeModeOn = false; // w�a�nie zatopili�my
                var shipToDelete =
                    fleetLeft
                        .FirstOrDefault(battleship => battleship.Size == currentlyBombardedShip.ShipCurrentSize);
                if (shipToDelete != null)
                {
                    fleetLeft.Remove(shipToDelete);
                    UpdateStatistics(shipToDelete);
                }
                currentlyBombardedShip = null;

            }
        }

        private void UpdateStatistics(IBattleship sunkShip)
        {
            //TODO
        }

        private static IList<ShipPosition> CreateUnusedPositions(int rows, int columns)
        {
            IList<ShipPosition> pos = new List<ShipPosition>(rows * columns);
            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= columns; j++)
                {
                    pos.Add(new ShipPosition(i, j));
                }
            }
            return pos;
        }

        private BombardedBattleshipInfo GuessBattleship(ShipPosition position)
        {
            // rozkmini� takie rzeczy jak odleg�o�ci od kraw�dzi, od innych statk�w, por�wna� z zatopion� flot�.
            //narazie kompletna kupa
            return new BombardedBattleshipInfo(currentBoard.GetLength(1), currentBoard.GetLength(0))
            {
                ShipCurrentSize = 1,

                PotencialOrientation =
                    orientationStatistics[ShipOrientation.Horizontal] > orientationStatistics[ShipOrientation.Vertical]
                        ? ShipOrientation.Horizontal
                        : ShipOrientation.Vertical,
                
            };
        }
    }
}