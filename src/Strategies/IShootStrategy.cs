﻿using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer.Strategies
{
    internal interface IShootStrategy
    {
        void UpdateHistory(ShipPosition position, bool wasHit, bool isSunk);

        ShipPosition GetNextPosition();
    }
}
