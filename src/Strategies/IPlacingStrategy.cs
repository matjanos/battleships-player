﻿using System.Collections.Generic;
using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer.Strategies
{
    interface IPlacingStrategy
    {
        void PlaceFleet(IGameplayBoard board, IList<IBattleship> fleet);
    }
}
