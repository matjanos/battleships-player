﻿using System;
using System.Collections.Generic;
using System.Linq;
using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer
{
    public class BombardedBattleshipInfo
    {
        private readonly int maxHorizontal;
        private readonly int maxVertical;
        public uint ShipCurrentSize { get; set; }

        public ShipOrientation? Orientation; // null if unknown yet

        public ShipOrientation PotencialOrientation; // null if unknown yet

        private readonly IList<ShipPosition> shipPositions = new List<ShipPosition>();

        public BombardedBattleshipInfo(int maxHorizontal, int maxVertical)
        {
            this.maxHorizontal = maxHorizontal;
            this.maxVertical = maxVertical;
        }

        public void SaveResult(ShipPosition shipPosition, bool isHit)
        {
            if (isHit)
            {
                if (shipPositions.Contains(shipPosition))
                {
                    throw new Exception("Coś nie gra...");
                }
                else
                {
                    shipPositions.Add(shipPosition);
                }
                if (shipPositions.Count == 2)//znamy orientacje
                {
                    if (shipPositions[0].GetLeft().Equals(shipPositions[1]) || shipPositions[0].GetRight().Equals(shipPositions[1]))
                        Orientation = ShipOrientation.Horizontal;
                    else
                        Orientation = ShipOrientation.Vertical;
                }

                if (shipPositions.OrderByDescending(position => position.Column).First().GetRight().Column > maxHorizontal)
                {
                    rightBorderAchived = true;
                }
                if (shipPositions.OrderBy(position => position.Column).First().GetLeft().Column == 0)
                {
                    leftBorderAchived = true;
                }
                if (shipPositions.OrderByDescending(position => position.Row).First().GetBottom().Row > maxVertical)
                {
                    bottomBorderAchived = true;
                }
                if (shipPositions.OrderBy(position => position.Row).First().GetUpper().Row == 0)
                {
                    topBorderAchived = true;
                }
            }
            else
            {
                if (shipPositions.OrderByDescending(position => position.Column).First().GetRight().Equals(shipPosition))
                {
                    rightBorderAchived = true;
                }
                if (shipPositions.OrderBy(position => position.Column).First().GetLeft().Equals(shipPosition))
                {
                    leftBorderAchived = true;
                }
                if (shipPositions.OrderByDescending(position => position.Row).First().GetBottom().Equals(shipPosition))
                {
                    bottomBorderAchived = true;
                }
                if (shipPositions.OrderBy(position => position.Row).First().GetUpper().Equals(shipPosition))
                {
                    topBorderAchived = true;
                }
            }
        }

        public ShipPosition GetNextPosition()
        {
            if (!Orientation.HasValue)
            {
                return GetShipPosition(PotencialOrientation);
            }
            else
            {
                return GetShipPosition(Orientation.Value);
            }
        }

        private ShipPosition GetShipPosition(ShipOrientation orientation)
        {
            if (bottomBorderAchived&&topBorderAchived&&leftBorderAchived&&rightBorderAchived)
                throw new Exception("Niemożliwe. chyba że jednomasztowiec.");

            ShipPosition position;
            if (orientation == ShipOrientation.Horizontal)
            {
                if (!leftBorderAchived)
                    position = shipPositions.OrderBy(pos => pos.Column).First().GetLeft();
                else if (!rightBorderAchived)
                    position = shipPositions.OrderByDescending(pos => pos.Column).First().GetRight();
                else
                {
                    PotencialOrientation = ShipOrientation.Vertical;
                    position = GetShipPosition(PotencialOrientation);
                }
            }
            else
            {
                if (!topBorderAchived)
                    position = shipPositions.OrderBy(pos => pos.Row).First().GetUpper();
                else if (!bottomBorderAchived)
                    position = shipPositions.OrderByDescending(pos => pos.Row).First().GetBottom();
                else
                {
                    PotencialOrientation = ShipOrientation.Horizontal;
                    position = GetShipPosition(PotencialOrientation);
                }
            }

            return position;
        }

        private bool leftBorderAchived;
        private bool topBorderAchived;
        private bool bottomBorderAchived;
        private bool rightBorderAchived;
    }
}