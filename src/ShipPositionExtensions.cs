﻿using System;
using System.Collections.Generic;
using Battleships.Gameplay;

namespace Battleships.Modules.KubaPlayer
{
    public static class ShipPositionExtensions
    {
        public static ShipPosition GetLeft(this ShipPosition pos)
        {
            return new ShipPosition(pos.Row, pos.Column - 1);
        }

        public static ShipPosition GetRight(this ShipPosition pos)
        {
            return new ShipPosition(pos.Row, pos.Column + 1);
        }

        public static ShipPosition GetUpper(this ShipPosition pos)
        {
            return new ShipPosition(pos.Row - 1, pos.Column);
        }

        public static ShipPosition GetBottom(this ShipPosition pos)
        {
            return new ShipPosition(pos.Row + 1, pos.Column);
        }
    }

    public static class ListExtensions
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}