﻿using System;
using Battleships.Players;
using log4net.Config;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Unity;

namespace Battleships.Modules.KubaPlayer
{
    public class KubaPlayerModule: IModule
    {
        public KubaPlayerModule(IUnityContainer container)
        {
            Container = container;
            BasicConfigurator.Configure(); 
            XmlConfigurator.Configure();
        }

        public IUnityContainer Container { get; private set; }

        public void Initialize()
        {
            var repo = Container.Resolve<IGameplayPlayerRepository>();
            repo.RegisterPlayer<KubaPlayer>(KubaPlayer.PlayerName);
        }
    }
}
